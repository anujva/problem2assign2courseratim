#include <iostream>
#include <fstream>
#include <vector>
#include <climits>
#include <cstring>

using namespace std;

class Graph{
private:
	vector<int> nodes;
	int noOfBits;
	int noOfNodes;

public:
	Graph(fstream &myfile);
};

Graph::Graph(fstream &myfile){
	myfile.seekp(0);
	myfile>>noOfNodes>>noOfBits;
	
	while(myfile.good()){
		int* inputLine = new int[noOfBits];
		int sum = 0;
		for(int i=noOfBits-1; i>-1; i--){
			myfile>>inputLine[i];
			sum += inputLine[i]*pow(2.0, i);
		}
		nodes.push_back(sum);
	}
	
}

int main(){
	fstream myfile;
	myfile.open("clustering_Big.txt", ios::in);
	Graph graph(myfile);
}
